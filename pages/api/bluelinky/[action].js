import BlueLinky from 'bluelinky';

export default function handler(req, res) {
    const { action, pin } = req.query;
    let vehicle;
    console.log(pin);

    try {
        const client = new BlueLinky({
            username: process.env.BLUELINK_EMAIL,
            password: process.env.BLUELINK_PASS,
            brand: process.env.BLUELINK_BRAND,
            region: process.env.BLUELINK_REGION,
            pin: pin,
        });
        client.on('ready', async () => {
            vehicle = client.getVehicle(process.env.BLUELINK_VIN);
            let response;
            switch (action) {
                case 'status':
                    response = await vehicle.status({
                        refresh: false,
                        parsed: true,
                    });

                    break;

                case 'lock':
                    response = await vehicle.lock();

                default:
                    response = { error: 'No action' };
                    break;
            }

            res.status(200).send(JSON.stringify(response, null, 2));
        });

        client.on('error', async (err) => {
            console.log(err);
            res.status(500).send(
                err.source.body.errorMessage || 'Screwed at client'
            );
        });
    } catch (err) {
        // log the error from the command invocation
        console.log(err);
        res.status(500).send(err.message || 'Screwed in catch');
    }
}
